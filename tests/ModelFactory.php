<?php
$factory->define(\STS\StorageConnect\Tests\TestUser::class, function($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});